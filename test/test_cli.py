# -*- coding: utf-8 -*-

from click.testing import CliRunner
import json
import pytest
import tuxsuite.cli
import tuxsuite.build

sample_token = "Q9qMlmkjkIuIGmEAw-Mf53i_qoJ8Z2eGYCmrNx16ZLLQGrXAHRiN2ce5DGlAebOmnJFp9Ggcq9l6quZdDTtrkw"
sample_url = "https://foo.bar.tuxbuild.com/v1"


def test_usage():
    """Test running cli() with no arguments"""
    runner = CliRunner()
    result = runner.invoke(tuxsuite.cli.cli, [])
    assert result.exit_code == 0
    assert "Usage" in result.output
    assert "Commands" in result.output


def test_build_no_args():
    """Test calling build() with no options"""
    runner = CliRunner()
    result = runner.invoke(tuxsuite.cli.build, [])
    assert result.exit_code == 2
    assert "Usage" in result.output
    assert "help" in result.output


def test_build_usage():
    """Test calling build() with --help"""
    runner = CliRunner()
    result = runner.invoke(tuxsuite.cli.build, ["--help"])
    assert result.exit_code == 0
    assert "Usage" in result.output
    assert "--toolchain" in result.output
    assert "--git-repo TEXT" in result.output


@pytest.fixture
def tuxsuite_config(tmp_path, monkeypatch, tuxauth):
    c = tmp_path / "config.ini"
    with c.open("w") as f:
        f.write("[default]\n")
        f.write(f"token={sample_token}\n")
        f.write(f"api_url={sample_url}\n")
    monkeypatch.setenv("TUXSUITE_CONFIG", str(c))
    return c


def test_build(mocker, tuxsuite_config):
    build = mocker.patch("tuxsuite.Build.build")
    wait_for_object = mocker.patch("tuxsuite.cli.wait_for_object")
    runner = CliRunner()
    result = runner.invoke(
        tuxsuite.cli.build,
        [
            "--git-repo=https://git.example.com/linux.git",
            "--git-ref=master",
            "--target-arch=arm64",
            "--kconfig=defconfig",
            "--toolchain=gcc-9",
        ],
    )
    assert result.exit_code == 0
    assert build.call_count == 1
    assert wait_for_object.call_count == 1


def test_build_quiet(mocker, tuxsuite_config):
    Build = mocker.patch("tuxsuite.Build")
    Build.return_value.build_data = "https://tuxsuite.example.com/abcdef0123456789/"
    mocker.patch("tuxsuite.cli.wait_for_object")
    runner = CliRunner()
    result = runner.invoke(
        tuxsuite.cli.build,
        [
            "--git-repo=https://git.example.com/linux.git",
            "--git-ref=master",
            "--target-arch=arm64",
            "--kconfig=defconfig",
            "--toolchain=gcc-9",
            "--quiet",
        ],
    )
    assert result.exit_code == 0
    assert "Building Linux Kernel" not in result.output
    assert result.output == "https://tuxsuite.example.com/abcdef0123456789/\n"


def test_build_git_sha(mocker, tuxsuite_config):
    build = mocker.patch("tuxsuite.Build.build")
    wait_for_object = mocker.patch("tuxsuite.cli.wait_for_object")
    runner = CliRunner()
    result = runner.invoke(
        tuxsuite.cli.build,
        [
            "--git-repo=https://git.example.com/linux.git",
            "--git-sha=beefbee",
            "--target-arch=arm64",
            "--kconfig=defconfig",
            "--toolchain=gcc-9",
        ],
    )
    assert result.exit_code == 0
    assert build.call_count == 1
    assert wait_for_object.call_count == 1


def test_build_kernel_image(mocker, tuxsuite_config):
    build = mocker.patch("tuxsuite.Build.build")
    wait_for_object = mocker.patch("tuxsuite.cli.wait_for_object")
    runner = CliRunner()
    result = runner.invoke(
        tuxsuite.cli.build,
        [
            "--git-repo=https://git.example.com/linux.git",
            "--git-sha=beefbee",
            "--target-arch=arm64",
            "--kconfig=defconfig",
            "--toolchain=gcc-9",
            "--kernel-image=Image",
        ],
    )
    assert result.exit_code == 0
    assert build.call_count == 1
    assert wait_for_object.call_count == 1


def test_build_git_head(mocker, tuxsuite_config):
    get_git_head = mocker.patch("tuxsuite.gitutils.get_git_head")
    get_git_head.return_value = ("https://example.com/linux.git", "deadbeef")
    Build = mocker.patch("tuxsuite.Build")
    Build.return_value.build_data = "https://tuxsuite.example.com/abcdef0123456789/"
    wait_for_object = mocker.patch("tuxsuite.cli.wait_for_object")

    runner = CliRunner()
    result = runner.invoke(
        tuxsuite.cli.build,
        [
            "--git-head",
            "--target-arch=arm64",
            "--kconfig=defconfig",
            "--toolchain=gcc-9",
        ],
    )
    Build.assert_called_with(
        git_repo="https://example.com/linux.git",
        git_sha="deadbeef",
        git_ref=None,
        target_arch="arm64",
        kconfig=("defconfig",),
        toolchain="gcc-9",
        environment=(),
        targets=[],
        make_variables={},
        build_name=None,
        kernel_image=None,
    )
    wait_for_object.assert_called()
    assert result.exit_code == 0


def test_build_download(mocker, tuxsuite_config):
    build = mocker.patch("tuxsuite.Build.build")
    wait_for_object = mocker.patch("tuxsuite.cli.wait_for_object")
    download = mocker.patch("tuxsuite.download.download")
    runner = CliRunner()
    result = runner.invoke(
        tuxsuite.cli.build,
        [
            "--git-repo=https://git.example.com/linux.git",
            "--git-ref=master",
            "--target-arch=arm64",
            "--kconfig=defconfig",
            "--toolchain=gcc-9",
            "--download",
        ],
    )
    assert result.exit_code == 0
    assert build.call_count == 1
    assert wait_for_object.call_count == 1
    download.assert_called_with(mocker.ANY, ".")


def test_build_download_output_dir(mocker, tuxsuite_config, tmp_path):
    mocker.patch("tuxsuite.Build.build")
    mocker.patch("tuxsuite.cli.wait_for_object")
    download = mocker.patch("tuxsuite.download.download")
    runner = CliRunner()
    result = runner.invoke(
        tuxsuite.cli.build,
        [
            "--git-repo=https://git.example.com/linux.git",
            "--git-ref=master",
            "--target-arch=arm64",
            "--kconfig=defconfig",
            "--toolchain=gcc-9",
            "--download",
            f"--output-dir={tmp_path}",
        ],
    )
    assert result.exit_code == 0
    download.assert_called_with(mocker.ANY, str(tmp_path))


def test_build_show_logs(mocker, tuxsuite_config):
    Build = mocker.patch("tuxsuite.Build")
    build = Build.return_value
    build.build_data = "https://builds.com/21321312312/"
    wait_for_object = mocker.patch("tuxsuite.cli.wait_for_object")
    download_file = mocker.patch("tuxsuite.download.download_file")
    runner = CliRunner()
    result = runner.invoke(
        tuxsuite.cli.build,
        [
            "--git-repo=https://git.example.com/linux.git",
            "--git-ref=master",
            "--target-arch=arm64",
            "--kconfig=defconfig",
            "--toolchain=gcc-9",
            "--show-logs",
        ],
    )
    assert result.exit_code == 0
    assert build.build.call_count == 1
    assert wait_for_object.call_count == 1
    download_file.assert_called_with(
        "https://builds.com/21321312312/build.log", mocker.ANY
    )


def test_build_download_show_logs(mocker, tuxsuite_config, tmp_path):
    Build = mocker.patch("tuxsuite.Build")
    build = Build.return_value
    build.uid = "21321312312"
    build.build_data = "https://builds.com/21321312312/"
    wait_for_object = mocker.patch("tuxsuite.cli.wait_for_object")
    runner = CliRunner()
    (tmp_path / "21321312312").mkdir()
    (tmp_path / "21321312312" / "build.log").write_text(
        "log line 1\nlog line 2\nerror: something\n"
    )
    result = runner.invoke(
        tuxsuite.cli.build,
        [
            "--git-repo=https://git.example.com/linux.git",
            "--git-ref=master",
            "--target-arch=arm64",
            "--kconfig=defconfig",
            "--toolchain=gcc-9",
            "--download",
            f"--output-dir={tmp_path}",
            "--show-logs",
        ],
    )
    assert result.exit_code == 0
    assert build.build.call_count == 1
    assert wait_for_object.call_count == 1
    assert "log line 1\nlog line 2\n" in result.output
    assert "error: something" in result.output


sample_build_set = """
sets:
  - name: test
    builds:
      - {target_arch: arm64, toolchain: gcc-9, kconfig: defconfig}
      - {target_arch: arm64, toolchain: gcc-9, kconfig: allmodconfig}
      - {target_arch: arm64, toolchain: gcc-9, kconfig: allyesconfig}
      - {target_arch: arm, toolchain: gcc-9, kconfig: allmodconfig}
      - {target_arch: x86_64, toolchain: gcc-9, kconfig: allmodconfig}
      - {target_arch: x86_64, toolchain: clang-9, kconfig: allmodconfig}
      - {target_arch: x86_64, toolchain: gcc-9, kconfig: allyesconfig}
      - {target_arch: i386, toolchain: gcc-9, kconfig: allmodconfig}
      - {target_arch: riscv, toolchain: gcc-9, kconfig: allyesconfig}
  - name: arch-matrix
    builds:
      - {target_arch: arm64,  toolchain: gcc-9}
      - {target_arch: arm,    toolchain: gcc-9}
      - {target_arch: i386,   toolchain: gcc-9}
      - {target_arch: riscv,  toolchain: gcc-9}
      - {target_arch: x86_64,    toolchain: gcc-9}
"""


@pytest.fixture
def tux_config(tmp_path, tuxauth):
    config = tmp_path / "buildset.yaml"
    with config.open("w") as f:
        f.write(sample_build_set)
    return config


sample_plan = """
version: 1
name: Simple plan
description: A simple plan
jobs:

- name: tinyconfig
  builds:
    - {toolchain: gcc-8, target_arch: i386, kconfig: tinyconfig}
    - {toolchain: gcc-9, target_arch: i386, kconfig: tinyconfig}
  test: {device: qemu-i386, tests: [ltp-smoke]}
"""


@pytest.fixture
def plan_config(tmp_path, tuxauth):
    config = tmp_path / "plan.yaml"
    with config.open("w") as f:
        f.write(sample_plan)
    return config


def test_build_set(mocker, tuxsuite_config, tux_config):
    build = mocker.patch("tuxsuite.BuildSet.build")
    wait_for_object = mocker.patch("tuxsuite.cli.wait_for_object")
    runner = CliRunner()
    result = runner.invoke(
        tuxsuite.cli.build_set,
        [
            "--git-repo=https://git.example.com/linux.git",
            "--git-ref=master",
            f"--tux-config={tux_config}",
            "--set-name=test",
        ],
    )
    assert result.exit_code == 0
    assert build.call_count == 1
    assert wait_for_object.call_count == 1


def test_build_set_no_kconfig(mocker, tuxsuite_config, tux_config):
    build = mocker.patch("tuxsuite.BuildSet.build")
    wait_for_object = mocker.patch("tuxsuite.cli.wait_for_object")
    runner = CliRunner()
    result = runner.invoke(
        tuxsuite.cli.build_set,
        [
            "--git-repo=https://git.example.com/linux.git",
            "--git-ref=master",
            f"--tux-config={tux_config}",
            "--set-name=arch-matrix",
            "--quiet",
        ],
    )
    build.assert_not_called()
    wait_for_object.assert_not_called()
    assert result.exit_code == 1
    assert "kconfig is mandatory" in result.output


def test_build_set_quiet(mocker, tuxsuite_config, tux_config):
    BuildSet = mocker.patch("tuxsuite.BuildSet")
    builds = []
    for i in range(1, 10):
        build = mocker.MagicMock()
        build.build_data = f"https://tuxsuite.example.com/{i}/"
        builds.append(build)
    BuildSet.return_value.builds = builds
    mocker.patch("tuxsuite.cli.wait_for_object")
    runner = CliRunner()
    result = runner.invoke(
        tuxsuite.cli.build_set,
        [
            "--git-repo=https://git.example.com/linux.git",
            "--git-ref=master",
            f"--tux-config={tux_config}",
            "--set-name=test",
            "--quiet",
        ],
    )
    assert result.exit_code == 0
    output = "".join([f"https://tuxsuite.example.com/{i}/\n" for i in range(1, 10)])
    assert result.output == output


def test_build_set_git_sha(mocker, tuxsuite_config, tux_config):
    build = mocker.patch("tuxsuite.BuildSet.build")
    wait_for_object = mocker.patch("tuxsuite.cli.wait_for_object")
    runner = CliRunner()
    result = runner.invoke(
        tuxsuite.cli.build_set,
        [
            "--git-repo=https://git.example.com/linux.git",
            "--git-sha=beefbee",
            f"--tux-config={tux_config}",
            "--set-name=test",
            "--quiet",
        ],
    )
    assert result.exit_code == 0
    assert build.call_count == 1
    assert wait_for_object.call_count == 1


def test_build_set_download(mocker, tuxsuite_config, tux_config):
    build = mocker.patch("tuxsuite.BuildSet.build")
    wait_for_object = mocker.patch("tuxsuite.cli.wait_for_object")
    download = mocker.patch("tuxsuite.download.download")
    runner = CliRunner()
    result = runner.invoke(
        tuxsuite.cli.build_set,
        [
            "--git-repo=https://git.example.com/linux.git",
            "--git-ref=master",
            f"--tux-config={tux_config}",
            "--set-name=test",
            "--download",
        ],
    )
    assert result.exit_code == 0
    assert build.call_count == 1
    assert wait_for_object.call_count == 1
    assert download.call_count == 9


def test_build_set_show_logs(mocker, tuxsuite_config, tux_config):
    build = mocker.patch("tuxsuite.BuildSet.build")
    wait_for_object = mocker.patch("tuxsuite.cli.wait_for_object")
    show_log = mocker.patch("tuxsuite.cli.show_log")
    runner = CliRunner()
    result = runner.invoke(
        tuxsuite.cli.build_set,
        [
            "--git-repo=https://git.example.com/linux.git",
            "--git-ref=master",
            f"--tux-config={tux_config}",
            "--set-name=test",
            "--show-logs",
        ],
    )
    assert result.exit_code == 0
    assert build.call_count == 1
    assert wait_for_object.call_count == 1
    assert show_log.call_count == 9


def state(mocker, **kwargs):
    s = mocker.MagicMock()

    # defaults
    s.state = "completed"
    s.status = "pass"
    s.icon = "✓"
    s.cli_color = "white"
    s.errors = 0
    s.warnings = 0
    s.final = True

    for k, v in kwargs.items():
        setattr(s, k, v)

    return s


@pytest.fixture
def build_state(mocker):
    return state(mocker)


def test_wait_for_object_pass(mocker, build_state):
    build = mocker.MagicMock()
    build.watch.return_value = [build_state]
    assert tuxsuite.cli.wait_for_object(build)


def test_wait_for_object_pass_with_warnings(mocker, build_state):
    build = mocker.MagicMock()
    build_state.warnings = 1
    build.watch.return_value = [build_state]
    assert tuxsuite.cli.wait_for_object(build)


def test_wait_for_object_fail(mocker, build_state):
    build = mocker.MagicMock()
    build_state.status = "fail"
    build_state.errors = 1
    build.watch.return_value = [build_state]
    assert not tuxsuite.cli.wait_for_object(build)


def test_wait_for_object_infra_failure(mocker, build_state):
    build = mocker.MagicMock()
    build_state.state = "error"
    build.final = True
    build.watch.return_value = [build_state]
    assert not tuxsuite.cli.wait_for_object(build)


def test_wait_for_object_infra_failure_retried(mocker):
    error = state(mocker, state="error", final=False)
    retry_pass = state(mocker)
    build = mocker.MagicMock()
    build.watch.return_value = [error, retry_pass]
    assert tuxsuite.cli.wait_for_object(build)


def test_build_environment_option():
    """Test calling build with --help to see environment option"""
    runner = CliRunner()
    result = runner.invoke(tuxsuite.cli.build, ["--help"])
    assert result.exit_code == 0
    assert "-e, --environment KEY_VALUE" in result.output


def test_build_valid_environment(mocker, tuxsuite_config):
    Build = mocker.patch("tuxsuite.Build")
    build = Build.return_value
    wait_for_object = mocker.patch("tuxsuite.cli.wait_for_object")
    runner = CliRunner()
    result = runner.invoke(
        tuxsuite.cli.build,
        [
            "--git-repo=https://git.example.com/linux.git",
            "--git-ref=master",
            "--target-arch=arm64",
            "--kconfig=defconfig",
            "--toolchain=gcc-9",
            "-e KCONFIG_ALLCONFIG=arch/arm64/configs/defconfig",
        ],
    )
    assert result.exit_code == 0
    assert build.build.call_count == 1
    assert wait_for_object.call_count == 1


def test_invalid_environment_key_value(mocker, tuxsuite_config):
    Build = mocker.patch("tuxsuite.Build")
    build = Build.return_value
    runner = CliRunner()
    result = runner.invoke(
        tuxsuite.cli.build,
        [
            "--git-repo=https://git.example.com/linux.git",
            "--git-ref=master",
            "--target-arch=arm64",
            "--kconfig=defconfig",
            "--toolchain=gcc-9",
            "-e INVALID",
        ],
    )
    assert result.exit_code == 1
    assert build.build.call_count == 0
    assert "Key Value pair not valid:  INVALID" in result.output


def test_build_make_targets_vars_argument():
    """Test calling build with --help to see targets argument"""
    runner = CliRunner()
    result = runner.invoke(tuxsuite.cli.build, ["--help"])
    assert result.exit_code == 0
    assert "[VAR=VALUE...] [target ...]" in result.output
    assert "[KEY=VALUE | target] ..." in result.output


def test_build_set_make_targets_vars_argument():
    """Test calling build with --help to see targets argument"""
    runner = CliRunner()
    result = runner.invoke(tuxsuite.cli.build_set, ["--help"])
    assert result.exit_code == 0
    assert "[VAR=VALUE...] [target ...]" in result.output
    assert "[KEY=VALUE | target] ..." in result.output


def test_build_valid_make_targets(mocker, tuxsuite_config):
    Build = mocker.patch("tuxsuite.Build")
    build = Build.return_value
    wait_for_object = mocker.patch("tuxsuite.cli.wait_for_object")
    runner = CliRunner()
    result = runner.invoke(
        tuxsuite.cli.build,
        [
            "--git-repo=https://git.example.com/linux.git",
            "--git-ref=master",
            "--target-arch=arm64",
            "--kconfig=defconfig",
            "--toolchain=gcc-9",
            "dtbs config",
        ],
    )
    assert result.exit_code == 0
    assert build.build.call_count == 1
    assert wait_for_object.call_count == 1


def test_build_valid_make_vars(mocker, tuxsuite_config):
    Build = mocker.patch("tuxsuite.Build")
    build = Build.return_value
    wait_for_object = mocker.patch("tuxsuite.cli.wait_for_object")
    runner = CliRunner()
    result = runner.invoke(
        tuxsuite.cli.build,
        [
            "--git-repo=https://git.example.com/linux.git",
            "--git-ref=master",
            "--target-arch=arm64",
            "--kconfig=defconfig",
            "--toolchain=gcc-9",
            "LLVM=1",
        ],
    )
    assert result.exit_code == 0
    assert build.build.call_count == 1
    assert wait_for_object.call_count == 1


def test_build_invalid_make_vars(mocker, tuxsuite_config):
    Build = mocker.patch("tuxsuite.Build")
    build = Build.return_value
    wait_for_object = mocker.patch("tuxsuite.cli.wait_for_object")
    runner = CliRunner()
    result = runner.invoke(
        tuxsuite.cli.build,
        [
            "--git-repo=https://git.example.com/linux.git",
            "--git-ref=master",
            "--target-arch=arm64",
            "--kconfig=defconfig",
            "--toolchain=gcc-9",
            "LLVM=1=1",
        ],
    )
    assert result.exit_code == 1
    assert build.build.call_count == 0
    assert wait_for_object.call_count == 0


def test_plan_no_args():
    runner = CliRunner()
    result = runner.invoke(tuxsuite.cli.plan, [])
    assert result.exit_code == 2
    assert "Usage" in result.output
    assert "help" in result.output


def test_plan_usage():
    """Test calling plan() with --help"""
    runner = CliRunner()
    result = runner.invoke(tuxsuite.cli.plan, ["--help"])
    assert result.exit_code == 0
    assert "Usage" in result.output
    assert "--git-repo" in result.output
    assert "--name" in result.output
    assert "--description" in result.output


def test_plan_cli(mocker, plan_config):
    plan = mocker.patch("tuxsuite.Plan")
    mocker.patch("tuxsuite.cli.wait_for_object")
    runner = CliRunner()
    result = runner.invoke(
        tuxsuite.cli.plan,
        [
            "--git-repo=https://git.example.com/linux.git",
            "--git-ref=master",
            str(plan_config),
        ],
    )
    assert result.exit_code == 0
    plan.assert_called_once()


def test_plan_cli_options(mocker, plan_config, tuxsuite_config):
    download = mocker.patch("tuxsuite.download.download")
    plan_obj = tuxsuite.Plan(
        "", token="token", tuxapi_url="http://api", project="prj", group="grp"
    )
    plan_obj.builds = [mocker.Mock(uid="1"), mocker.Mock(uid="2")]
    plan_obj.tests = [
        mocker.Mock(uid="1", wait_for="1"),
        mocker.Mock(uid="2", wait_for=None),
    ]
    plan_obj.submit = mocker.Mock()
    plan = mocker.patch("tuxsuite.Plan")
    plan.return_value = plan_obj
    plan_obj.results = {
        "builds": {
            "1": tuxsuite.build.BuildState(
                build=None,
                state="finished",
                status="",
                message="build 1 pass",
                icon="🎉",
                cli_color="green",
            ),
            "2": tuxsuite.build.BuildState(
                build=None,
                state="finished",
                status="",
                message="build 2 pass",
                icon="🎉",
                cli_color="green",
            ),
        },
        "tests": {
            "1": tuxsuite.build.BuildState(
                build=None,
                state="finished",
                status="",
                message="test 1 pass",
                icon="🎉",
                cli_color="green",
            ),
            "2": tuxsuite.build.BuildState(
                build=None,
                state="finished",
                status="",
                message="test 2 pass",
                icon="🎉",
                cli_color="green",
            ),
        },
    }

    show_log = mocker.patch("tuxsuite.cli.show_log")
    wait_for_object = mocker.patch("tuxsuite.cli.wait_for_object")
    runner = CliRunner()
    result = runner.invoke(
        tuxsuite.cli.plan,
        [
            "--git-repo=https://git.example.com/linux.git",
            "--git-ref=master",
            "--download",
            "--show-logs",
            str(plan_config),
        ],
    )

    assert result.exit_code == 0
    plan.assert_called_once()
    assert wait_for_object.call_count == 1
    assert download.call_count == 2
    assert show_log.call_count == 2

    assert (
        """
Summary: http://api/v1/groups/grp/projects/prj
* 🎉 build 1 pass: None
  -> 🎉 test 1 pass: None
* 🎉 build 2 pass: None
* 🎉 test 2 pass: None"""
        in result.output
    )


def test_plan_empty_config(mocker, plan_config, tuxsuite_config):
    runner = CliRunner()
    result = runner.invoke(
        tuxsuite.cli.plan,
        [
            "--git-repo=https://git.example.com/linux.git",
            "--git-ref=master",
            "--job-name=kconfig",
            str(plan_config),
        ],
    )
    assert result.exit_code == 0
    assert "Empty plan, skipping" in result.output


def test_test_no_args():
    runner = CliRunner()
    result = runner.invoke(tuxsuite.cli.test, [])
    assert result.exit_code == 2
    assert "Usage" in result.output
    assert "help" in result.output


def test_test_usage():
    """Test calling test() with --help"""
    runner = CliRunner()
    result = runner.invoke(tuxsuite.cli.test, ["--help"])
    assert result.exit_code == 0
    assert "Usage" in result.output
    assert "--device" in result.output
    assert "--kernel" in result.output


def test_test_cli(mocker, tuxsuite_config):
    test = mocker.patch("tuxsuite.Test")
    wait_for_object = mocker.patch("tuxsuite.cli.wait_for_object")
    runner = CliRunner()
    result = runner.invoke(
        tuxsuite.cli.test,
        [
            "--device=qemu-arm64",
            "--kernel=https://example.com/vmlinux",
        ],
    )
    assert result.exit_code == 0
    assert test.call_count == 1
    test.assert_called_once_with(
        device="qemu-arm64",
        kernel="https://example.com/vmlinux",
        modules=None,
        tests=[],
        boot_args=None,
        wait_for=None,
    )
    assert wait_for_object.call_count == 1

    mocker.resetall()
    result = runner.invoke(
        tuxsuite.cli.test,
        [
            "--device=qemu-arm64",
            "--kernel=https://example.com/vmlinux",
            "--modules=https://example.com/modules.tar.xz",
            "--tests=ltp-smoke",
        ],
    )
    assert result.exit_code == 0
    assert test.call_count == 1
    test.assert_called_once_with(
        device="qemu-arm64",
        kernel="https://example.com/vmlinux",
        modules="https://example.com/modules.tar.xz",
        tests=["ltp-smoke"],
        boot_args=None,
        wait_for=None,
    )
    assert wait_for_object.call_count == 1

    mocker.resetall()
    result = runner.invoke(
        tuxsuite.cli.test,
        [
            "--device=qemu-arm64",
            "--tests=ltp-smoke",
            "--wait-for=mybuilduid",
        ],
    )
    assert result.exit_code == 0
    assert test.call_count == 1
    test.assert_called_once_with(
        device="qemu-arm64",
        kernel="",
        modules=None,
        tests=["ltp-smoke"],
        boot_args=None,
        wait_for="mybuilduid",
    )
    assert wait_for_object.call_count == 1


def test_test_cli_errors(mocker):
    test = mocker.patch("tuxsuite.Test")
    wait_for_object = mocker.patch("tuxsuite.cli.wait_for_object")
    runner = CliRunner()
    result = runner.invoke(
        tuxsuite.cli.test,
        [
            "--device=qemu-arm64",
            "--tests=something,ltp-smoke",
        ],
    )
    assert result.exit_code == 1
    assert test.call_count == 0
    assert wait_for_object.call_count == 0

    mocker.resetall()
    result = runner.invoke(
        tuxsuite.cli.test,
        [
            "--device=qemu-arm64",
            "--kernel=https://example.com/vmlinux",
            "--wait-for=mybuilduid",
        ],
    )
    assert result.exit_code == 1
    assert test.call_count == 0


def test_build_no_wait(mocker, tmp_path):
    build = mocker.patch("tuxsuite.Build.build")
    format_result = mocker.patch("tuxsuite.cli.format_result")
    runner = CliRunner()
    result = runner.invoke(
        tuxsuite.cli.build,
        [
            "--git-repo=https://git.example.com/linux.git",
            "--git-ref=master",
            "--target-arch=arm64",
            "--kconfig=defconfig",
            "--toolchain=gcc-9",
            "--no-wait",
            f"--json-out={tmp_path}/build_result.json",
        ],
    )
    assert result.exit_code == 0
    assert build.call_count == 1
    assert format_result.call_count == 1


def test_build_set_no_wait(mocker, tuxsuite_config, tux_config, tmp_path):
    build = mocker.patch("tuxsuite.BuildSet.build")
    format_result = mocker.patch("tuxsuite.cli.format_result")
    runner = CliRunner()
    result = runner.invoke(
        tuxsuite.cli.build_set,
        [
            "--git-repo=https://git.example.com/linux.git",
            "--git-ref=master",
            f"--tux-config={tux_config}",
            "--set-name=test",
            "--no-wait",
            f"--json-out={tmp_path}/build_result.json",
        ],
    )
    assert result.exit_code == 0
    assert build.call_count == 1
    assert format_result.call_count == 9


def test_test_cli_no_wait(mocker, tuxsuite_config):
    test = mocker.patch("tuxsuite.Test")
    format_result = mocker.patch("tuxsuite.cli.format_result")
    runner = CliRunner()
    result = runner.invoke(
        tuxsuite.cli.test,
        [
            "--device=qemu-arm64",
            "--kernel=https://example.com/vmlinux",
            "--no-wait",
        ],
    )
    assert result.exit_code == 0
    assert test.call_count == 1
    assert format_result.call_count == 1


def test_plan_cli_no_wait(mocker, tuxsuite_config, plan_config):
    plan = mocker.patch("tuxsuite.Plan")
    runner = CliRunner()
    result = runner.invoke(
        tuxsuite.cli.plan,
        [
            "--git-repo=https://git.kernel.org/pub/scm/linux/kernel/git/torvalds/linux.git",
            "--git-ref=https://example.com/vmlinux",
            f"{plan_config}",
            "--no-wait",
        ],
    )
    assert result.exit_code == 0
    assert plan.call_count == 1


sample_build_result = {
    "build_name": "",
    "download_url": "https://builds.dev.tuxbuild.com/1sMUTVjE25tQvDmjJ8SjWblpOJr/",
    "errors_count": 0,
    "kconfig": ["tinyconfig"],
    "plan": "null",
    "result": "pass",
    "state": "finished",
    "status_message": "build completed",
    "target_arch": "arm",
    "toolchain": "gcc-9",
    "uid": "1sMUTVjE25tQvDmjJ8SjWblpOJr",
    "warnings_count": 0,
}


sample_test_result = {
    "device": "qemu-i386",
    "plan": "1sMNB0p8a3OrpUmEBgYah0Ycer9",
    "result": "fail",
    "results": {"boot": "fail"},
    "state": "finished",
    "tests": ["boot"],
    "uid": "1sMNBWLFrsOahyXQCNeiT4bgysP",
}

sample_all_result = {
    "builds": {
        "count": 3,
        "next": "null",
        "results": [
            sample_build_result,
            {**sample_build_result, **dict({"warnings_count": 1})},
            {**sample_build_result, **dict({"warnings_count": 3})},
            {**sample_build_result, **dict({"result": "fail", "errors_count": 1})},
        ],
    },
    "plans": {
        "count": 1,
        "next": "null",
        "results": [
            {
                "description": "A simple plan",
                "name": "Simple plan",
                "project": "tuxsuite/senthil",
                "uid": "1sMNB0p8a3OrpUmEBgYah0Ycer9",
            }
        ],
    },
    "tests": {
        "count": 1,
        "next": "null",
        "results": [
            sample_test_result,
        ],
    },
}


sample_plan_status = {
    "builds": {
        "1sewrBhxNVbsURAKBjeXX8pyjwY": sample_build_result,
    },
    "tests": {
        "1sewrJ4GHQygVKvTjUxuYRK1hOh": sample_test_result,
    },
}


@pytest.fixture
def build_result(tmp_path):
    build_json = tmp_path / "build_result.json"
    with build_json.open("w") as f:
        f.write(json.dumps(sample_build_result))
    return build_json


@pytest.fixture
def test_result(tmp_path):
    test_json = tmp_path / "test_result.json"
    with test_json.open("w") as f:
        f.write(json.dumps(sample_test_result))
    return test_json


@pytest.fixture
def build_set_result(tmp_path):
    build_set_json = tmp_path / "build_set_result.json"
    build_set = [
        sample_build_result,
        sample_build_result,
    ]
    with build_set_json.open("w") as f:
        f.write(json.dumps(build_set))
    return build_set_json


@pytest.fixture
def plan_result(tmp_path):
    plan_json = tmp_path / "plan_result.json"
    with plan_json.open("w") as f:
        f.write(json.dumps(sample_plan_status))
    return plan_json


def test_results_cli(
    mocker,
    tmp_path,
    tuxsuite_config,
    build_result,
    test_result,
    build_set_result,
    plan_result,
):
    results = mocker.patch(
        "tuxsuite.Results.get_all", return_value=(sample_all_result, "http://api")
    )
    runner = CliRunner()
    result = runner.invoke(
        tuxsuite.cli.results,
        [],
    )
    assert result.exit_code == 0
    assert results.call_count == 1

    format_result = mocker.patch("tuxsuite.cli.format_result")
    mocker.resetall()
    results = mocker.patch(
        "tuxsuite.Results.get_build", return_value=sample_build_result
    )
    result = runner.invoke(
        tuxsuite.cli.results,
        [
            "--build=1sMUTVjE25tQvDmjJ8SjWblpOJr",
            f"--json-out={tmp_path}/build_result.json",
        ],
    )
    assert result.exit_code == 0
    assert results.call_count == 1
    assert format_result.call_count == 1

    mocker.resetall()
    results = mocker.patch(
        "tuxsuite.Results.get_test", return_value=(sample_test_result, "http://api")
    )
    result = runner.invoke(
        tuxsuite.cli.results,
        [
            "--test=1sMNBWLFrsOahyXQCNeiT4bgysP",
        ],
    )
    assert result.exit_code == 0
    assert results.call_count == 1
    assert format_result.call_count == 1

    mocker.resetall()
    results = mocker.patch(
        "tuxsuite.Results.get_plan", return_value=(sample_all_result, "http://api")
    )
    result = runner.invoke(
        tuxsuite.cli.results,
        [
            "--plan=1sMNB0p8a3OrpUmEBgYah0Ycer9",
        ],
    )
    assert result.exit_code == 0
    assert results.call_count == 1
    assert format_result.call_count == 6

    mocker.resetall()
    results = mocker.patch(
        "tuxsuite.Results.get_build", return_value=sample_build_result
    )
    result = runner.invoke(
        tuxsuite.cli.results,
        [
            f"--from-json={build_result}",
        ],
    )
    assert result.exit_code == 0
    assert results.call_count == 1
    assert format_result.call_count == 1

    mocker.resetall()
    results = mocker.patch(
        "tuxsuite.Results.get_test", return_value=(sample_test_result, "http://api")
    )
    result = runner.invoke(
        tuxsuite.cli.results,
        [
            f"--from-json={test_result}",
        ],
    )
    assert result.exit_code == 0
    assert results.call_count == 1
    assert format_result.call_count == 1

    mocker.resetall()
    results = mocker.patch(
        "tuxsuite.Results.get_build", return_value=sample_build_result
    )
    result = runner.invoke(
        tuxsuite.cli.results,
        [
            f"--from-json={build_set_result}",
            f"--json-out={tmp_path}/build_set_result.json",
        ],
    )
    assert result.exit_code == 0
    assert results.call_count == 2
    assert format_result.call_count == 2

    mocker.resetall()
    results = mocker.patch(
        "tuxsuite.Results.get_plan", return_value=(sample_all_result, "http://api")
    )
    result = runner.invoke(
        tuxsuite.cli.results,
        [
            f"--from-json={plan_result}",
        ],
    )
    assert result.exit_code == 0
    assert results.call_count == 1
    assert format_result.call_count == 6


def test_results_cli_errors(mocker, tmp_path, tuxsuite_config, build_set_result):
    results = mocker.patch(
        "tuxsuite.Results.get_all",
        return_value=(sample_all_result, "http://api"),
        side_effect=tuxsuite.exceptions.URLNotFound,
    )
    format_result = mocker.patch("tuxsuite.cli.format_result")
    runner = CliRunner()
    result = runner.invoke(
        tuxsuite.cli.results,
        [],
    )
    assert result.exit_code == 1
    assert results.call_count == 1
    assert format_result.call_count == 0

    mocker.resetall()
    results = mocker.patch(
        "tuxsuite.Results.get_build",
        return_value=sample_build_result,
        side_effect=tuxsuite.exceptions.URLNotFound,
    )
    result = runner.invoke(
        tuxsuite.cli.results,
        [
            "--build=1sMUTVjE25tQvDmjJ8SjWblpOJr",
        ],
    )
    assert result.exit_code == 1
    assert results.call_count == 1
    assert format_result.call_count == 0

    mocker.resetall()
    results = mocker.patch(
        "tuxsuite.Results.get_test",
        return_value=(sample_test_result, "http://api"),
        side_effect=tuxsuite.exceptions.URLNotFound,
    )
    result = runner.invoke(
        tuxsuite.cli.results,
        [
            "--test=1sMNBWLFrsOahyXQCNeiT4bgysP",
        ],
    )
    assert result.exit_code == 1
    assert results.call_count == 1
    assert format_result.call_count == 0

    mocker.resetall()
    results = mocker.patch(
        "tuxsuite.Results.get_plan",
        return_value=(sample_all_result, "http://api"),
        side_effect=tuxsuite.exceptions.URLNotFound,
    )
    result = runner.invoke(
        tuxsuite.cli.results,
        [
            "--plan=1sMNB0p8a3OrpUmEBgYah0Ycer9",
        ],
    )
    assert result.exit_code == 1
    assert results.call_count == 1
    assert format_result.call_count == 0

    mocker.resetall()
    results = mocker.patch(
        "tuxsuite.Results.get_build",
        return_value=sample_build_result,
        side_effect=tuxsuite.exceptions.URLNotFound,
    )
    result = runner.invoke(
        tuxsuite.cli.results,
        [
            f"--from-json={build_set_result}",
            f"--json-out={tmp_path}/build_set_result.json",
        ],
    )
    assert result.exit_code == 1
    assert results.call_count == 1
    assert format_result.call_count == 0

    mocker.resetall()
    results = mocker.patch(
        "tuxsuite.Results",
        side_effect=tuxsuite.exceptions.TuxSuiteError,
    )
    runner = CliRunner()
    result = runner.invoke(
        tuxsuite.cli.results,
        [],
    )
    assert result.exit_code == 1
    assert results.call_count == 1
