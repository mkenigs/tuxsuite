# -*- coding: utf-8 -*-

import requests
import tuxsuite.config
import pytest


@pytest.fixture(autouse=True)
def session(mocker):
    mocker.patch("requests.Session.get")
    mocker.patch("requests.Session.post")
    return requests.Session


@pytest.fixture
def response():
    r = requests.Response()
    r.status_code = 200
    return r


@pytest.fixture
def post(session, response):
    session.post.return_value = response
    return session.post


@pytest.fixture
def get(session, response):
    session.get.return_value = response
    return session.get


@pytest.fixture
def tuxauth(mocker):
    get = mocker.Mock(
        return_value=mocker.Mock(
            **{
                "status_code": 200,
                "json.return_value": {
                    "UserDetails": {"Groups": ["tuxsuite"], "Name": "tux"}
                },
            }
        )
    )
    mocker.patch("tuxsuite.config.requests.get", get)
    return get


@pytest.fixture
def sample_token():
    return "Q9qMlmkjkIuIGmEAw-Mf53i_qoJ8Z2eGYCmrNx16ZLLQGrXAHRiN2ce5DGlAebOmnJFp9Ggcq9l6quZdDTtrkw"


@pytest.fixture
def sample_url():
    return "https://foo.bar.tuxbuild.com/v1"


@pytest.fixture(autouse=True)
def home(monkeypatch, tmp_path):
    h = tmp_path / "HOME"
    h.mkdir()
    monkeypatch.setenv("HOME", str(h))
    return h


@pytest.fixture
def config(monkeypatch, sample_token, sample_url, tuxauth):
    monkeypatch.setenv("TUXSUITE_TOKEN", sample_token)
    monkeypatch.setenv("TUXSUITE_URL", sample_url)
    config = tuxsuite.config.Config("/nonexistent")
    config.kbapi_url = sample_url
    config.auth_token = sample_token
    return config


@pytest.fixture
def sample_plan_config():
    return """
version: 1
name: Simple plan
description: A simple plan
jobs:

- name: simple-gcc
  build: {toolchain: gcc-8, target_arch: i386, kconfig: tinyconfig}
  test: {device: qemu-i386, tests: [ltp-smoke]}

- name: full-gcc
  builds:
    - {toolchain: gcc-8, target_arch: i386, kconfig: tinyconfig}
    - {toolchain: gcc-9, target_arch: i386, kconfig: tinyconfig}
    - {toolchain: gcc-10, target_arch: i386, kconfig: tinyconfig}
  test: {device: qemu-i386, tests: [ltp-smoke]}

- builds:
    - {toolchain: clang-10, target_arch: i386, kconfig: tinyconfig}
    - {toolchain: clang-11, target_arch: i386, kconfig: tinyconfig}
    - {toolchain: clang-nightly, target_arch: i386, kconfig: tinyconfig}
  test: {device: qemu-i386}

- build: {toolchain: clang-nightly, target_arch: i386, kconfig: tinyconfig}
  tests:
    - {device: qemu-i386}
    - {device: qemu-i386, tests: [ltp-smoke]}

- builds:
    - {toolchain: gcc-8, target_arch: i386, kconfig: tinyconfig}
    - {toolchain: gcc-9, target_arch: i386, kconfig: tinyconfig}
    - {toolchain: gcc-10, target_arch: i386, kconfig: tinyconfig}
  tests:
    - {device: qemu-i386}
    - {device: qemu-i386, tests: [ltp-smoke]}

- tests:
    - {kernel: https://storage.tuxboot.com/arm64/Image, device: qemu-arm64, tests: [ltp-smoke]}
    - {kernel: https://storage.tuxboot.com/i386/bzImage, device: qemu-i386, tests: [ltp-smoke]}
    - {kernel: https://storage.tuxboot.com/mips64/vmlinux, device: qemu-mips64, tests: [ltp-smoke]}
    - {kernel: https://storage.tuxboot.com/ppc64/vmlinux, device: qemu-ppc64, tests: [ltp-smoke]}
    - {kernel: https://storage.tuxboot.com/riscv64/Image, device: qemu-riscv64, tests: [ltp-smoke]}
    - {kernel: https://storage.tuxboot.com/x86_64/bzImage, device: qemu-x86_64, tests: [ltp-smoke]}
"""


@pytest.fixture
def plan_config(tmp_path, sample_plan_config):
    c = tmp_path / "planv1.yml"
    c.write_text(sample_plan_config)
    return c
