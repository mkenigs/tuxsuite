# Devices

The following devices are currently supported by TuxTest:

* qemu-armv5
* qemu-armv7
* qemu-arm64
* qemu-i386
* qemu-mips64
* qemu-mips64el
* qemu-ppc64
* qemu-ppc64le
* qemu-riscv64
* qemu-sparc64
* qemu-x86_64

In order to be compatible with build architectures, the folowing aliases are also recognised:

* qemu-arm: qemu-armv7
* qemu-mips: qemu-mips64
* qemu-powerpc: qemu-ppc64
* qemu-riscv: qemu-riscv64
