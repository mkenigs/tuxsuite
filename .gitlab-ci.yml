image:
  name: python:3.7-buster

services:
  - docker:dind

stages:
  - test
  - build
  - build-docker
  - publish

test-tuxsuite:
  stage: test
  script:
    - pip install -r requirements.txt -r requirements-dev.txt
    - pip install -e . # For some reason, pytest-cov does not work without this
    - pytest --cov=tuxsuite .
    - flake8 .
    - black --check --diff .

test-tuxsuite-integration:
  stage: test
  image: python:3.7-buster
  before_script:
    - apt-get -q update
    - apt-get install -qy shunit2 git
  script:
    - python3 -m pip install -r requirements-dev.txt -e .
    - run-parts --report test/integration

test-markdown:
  stage: test
  image: docker.io/pipelinecomponents/markdownlint
  script:
    - scripts/markdownlint

build:
  stage: build
  script:
    - python setup.py sdist bdist_wheel
    - python setup.py --version > version.txt # save tuxsuite's version string
  artifacts:
    paths:
      - dist/
      - version.txt

publish-pypi:
  stage: publish
  script:
    - pip install -U twine
    - twine upload dist/*
  variables:
    TWINE_NON_INTERACTIVE: "true"
  only:
    - tags
  dependencies:
    - build

.docker:
  image: docker
  only:
    - tags
  before_script:
    - docker info
    - docker login -u $DOCKER_USER -p $DOCKER_PASSWORD
    - export tag="tuxsuite/tuxsuite:$(cat version.txt)${TAG_APPEND}"
    - export latest="tuxsuite/tuxsuite:latest${TAG_APPEND}"
  script:
    - docker build --pull --tag "${tag}" --tag "${latest}" .
    - docker push "${tag}"
    - docker push "${latest}"
  variables:
    DOCKER_DRIVER: overlay2

docker-amd64:
  extends: .docker
  stage: build-docker
  dependencies:
    - build
  variables:
    TAG_APPEND: -amd64

docker-arm64:
  extends: .docker
  stage: build-docker
  tags:
     - arm64-dind
  dependencies:
    - build
  variables:
    TAG_APPEND: -arm64

publish-dockerhub:
  extends: .docker
  stage: publish
  variables:
    DOCKER_CLI_EXPERIMENTAL: "enabled"
  script:
    - docker manifest create "${tag}" "${tag}-amd64" "${tag}-arm64"
    - docker manifest create "${latest}" "${latest}-amd64" "${latest}-arm64"
    - docker manifest push "${tag}"
    - docker manifest push "${latest}"

.docs:
  stage: build
  before_script:
    - python3 -m pip install mkdocs-material mkdocs-mermaid2-plugin
  script:
    - make public

build-docs:
  extends: .docs
  except:
    - master

pages:
  extends: .docs
  artifacts:
    paths:
      - public
  only:
    - master
