# -*- coding: utf-8 -*-

from attr import attrs, attrib
import concurrent.futures
import json
import queue
import os
import random
import re
import sys
import time
import traceback
import tuxsuite.exceptions
import tuxsuite.requests
from tuxsuite.version import __version__
import uuid


# We will poll for status change for an average duration of 180 minutes
state_timeout = 10800  # 60 * 180
delay_status_update = int(os.getenv("TUXSUITE_DELAY_STATUS_UPDATE", "29")) + 1


def post_request(url, headers, request):
    response = tuxsuite.requests.post(url, data=json.dumps(request), headers=headers)
    if response.ok:
        return json.loads(response.text)
    else:
        if response.status_code == 400:
            response_data = response.json()
            if "error" in response_data:
                message = response_data["error"]
            else:
                message = response_data.get("status_message")
            raise tuxsuite.exceptions.BadRequest(message)
        else:
            try:
                raise Exception(response.json()["error"])
            except Exception:
                response.raise_for_status()


def get_request(url, headers, params=None):
    response = tuxsuite.requests.get(url, headers=headers, params=params)
    if response.status_code == 200:
        return json.loads(response.text)
    else:
        if response.status_code == 404:
            raise tuxsuite.exceptions.URLNotFound(response.text)
        response.raise_for_status()  # Some unexpected status that's not caught


class HeadersMixin:
    @property
    def headers(self):
        return {
            "User-Agent": "tuxsuite/{}".format(__version__),
            "Content-Type": "application/json",
            "Authorization": self.token,
        }


@attrs(kw_only=True)
class BuildState:
    build = attrib()
    state = attrib()
    status = attrib(default=None)
    message = attrib()
    warnings = attrib(default=0)
    errors = attrib(default=0)
    icon = attrib()
    cli_color = attrib()
    final = attrib(default=False)


@attrs(kw_only=True)
class Build(HeadersMixin):
    git_repo = attrib()
    git_ref = attrib(default=None)
    git_sha = attrib(default=None)
    target_arch = attrib()
    kconfig = attrib()
    toolchain = attrib()
    token = attrib()
    kbapi_url = attrib()
    tuxapi_url = attrib()
    group = attrib()
    project = attrib()
    environment = attrib(default={})
    targets = attrib(default=[])
    make_variables = attrib(default={})
    kernel_image = attrib(default=None)

    build_data = attrib(default=None)
    build_name = attrib(default=None)
    status = attrib(default={})
    uid = attrib(default=None)

    def __attrs_post_init__(self):
        if isinstance(self.kconfig, str):
            self.kconfig = [self.kconfig]
        self.verify_build_parameters()
        self.client_token = str(uuid.uuid4())

    def __str__(self):
        git_short_log = self.status.get("git_short_log", "")

        if len(self.kconfig) > 1:
            kconfig = f"{self.kconfig[0]}+{len(self.kconfig)-1}"
        else:
            kconfig = self.kconfig[0]
        return "{} {} ({}) with {} @ {}".format(
            git_short_log,
            self.target_arch,
            kconfig,
            self.toolchain,
            self.build_data,
        )

    @staticmethod
    def is_supported_git_url(url):
        """
        Check that the git url provided is valid (namely, that it's not an ssh
        url)
        """
        return re.match(r"^(git://|https?://).*$", url) is not None

    def generate_build_request(self, plan=None):
        """return a build data in a python dict"""
        build_entry = {}
        build_entry["client_token"] = self.client_token
        build_entry["git_repo"] = self.git_repo
        if self.git_ref:
            build_entry["git_ref"] = self.git_ref
        if self.git_sha:
            build_entry["git_sha"] = self.git_sha
        build_entry["target_arch"] = self.target_arch
        build_entry["toolchain"] = self.toolchain
        build_entry["kconfig"] = self.kconfig
        build_entry["environment"] = dict(self.environment)
        build_entry["targets"] = self.targets
        build_entry["make_variables"] = self.make_variables
        if self.kernel_image:
            build_entry["kernel_image"] = self.kernel_image
        if self.build_name:
            build_entry["build_name"] = self.build_name
        if plan is not None:
            build_entry["plan"] = plan
        return build_entry

    def verify_build_parameters(self):
        """Pre-check the build arguments"""
        assert self.git_repo, "git_repo is mandatory"
        assert self.is_supported_git_url(
            self.git_repo
        ), "git url must be in the form of git:// or http:// or https://"
        assert (self.git_ref and not self.git_sha) or (
            self.git_sha and not self.git_ref
        ), "Either a git_ref or a git_sha is required"
        assert self.target_arch is not None, "target_arch is mandatory"
        assert self.kconfig, "kconfig is mandatory"
        assert self.toolchain, "toolchain is mandatory"
        assert self.headers is not None, "headers is mandatory"

    def build(self):
        """Submit the build request"""
        data = []
        data.append(self.generate_build_request())
        url = self.tuxapi_url + "/v1/groups/{}/projects/{}/builds".format(
            self.group, self.project
        )
        self.status = post_request(url, self.headers, data)[0]
        self.build_data = self.status["download_url"]
        self.uid = self.status["uid"]

    def get_status(self):
        """Fetches the build status and updates the values inside the build object"""
        url = self.tuxapi_url + "/v1/groups/{}/projects/{}/builds/{}".format(
            self.group, self.project, self.uid
        )
        self.status = get_request(url, self.headers)

    def watch(self, iterator=False):
        """
        Wait for the build to finish. Whenever the state changes, yielding the
        new state).

        Will timeout after `state_timeout` seconds.
        """
        attempts = 1
        max_attempts = 3
        waiting_states = {
            "queued": BuildState(
                build=self,
                state="queued",
                message="Queued",
                icon="⏳",
                cli_color="white",
            ),
            "provisioning": BuildState(
                build=self,
                state="provisioning",
                message="Provisioning",
                icon="⚙️ ",
                cli_color="white",
            ),
            "building": BuildState(
                build=self,
                state="building",
                message="Building",
                icon="🚀",
                cli_color="cyan",
            ),
            "running": BuildState(
                build=self,
                state="running",
                message="Running",
                icon="🚀",
                cli_color="cyan",
            ),
        }
        timeout = time.time() + state_timeout
        previous_state = None
        while True:
            self.get_status()
            state = self.status["state"]
            result = self.status["result"]
            if state != previous_state:
                if state in waiting_states:
                    yield waiting_states[state]
                elif (
                    state == "finished"
                    and result == "error"
                    and attempts < max_attempts
                ):
                    attempts += 1
                    message = self.status.get("status_message", "infrastructure error")
                    yield BuildState(
                        build=self,
                        state=self.status["tuxbuild_status"],
                        message=message
                        + f" - retrying (attempt {attempts}/{max_attempts})",
                        icon="🔧",
                        cli_color="yellow",
                    )
                    self.build()
                elif state == "finished":
                    break
                else:
                    # unknown state
                    yield BuildState(
                        build=self,
                        state=state,
                        message=self.status.get("status_message", state),
                        icon="🎰",
                        cli_color="yellow",
                    )
            elif time.time() >= timeout:
                raise tuxsuite.exceptions.Timeout(
                    f"Build timed out after {state_timeout/60} minutes; abandoning"
                )
            elif iterator:
                yield

            if not iterator:
                delay = random.randrange(delay_status_update)
                time.sleep(delay)
            previous_state = state

        errors = 0
        warnings = 0
        state = self.status["tuxbuild_status"]
        status = self.status["build_status"]

        if status == "pass":
            warnings = self.status["warnings_count"]
            if warnings == 0:
                icon = "🎉"
                message = "Pass"
                cli_color = "green"
            else:
                icon = "👾"
                cli_color = "yellow"
                if warnings == 1:
                    message = "Pass (1 warning)"
                else:
                    message = "Pass ({} warnings)".format(warnings)
        elif status == "fail":
            icon = "👹"
            cli_color = "bright_red"
            errors = self.status["errors_count"]
            if errors == 1:
                message = "Fail (1 error)"
            else:
                message = "Fail ({} errors)".format(errors)
            error_message = self.status.get("status_message")
            if error_message:
                message += f" with status message '{error_message}'"
        else:
            icon = "🔧"
            cli_color = "bright_red"
            message = self.status["status_message"]

        finished = BuildState(
            build=self,
            state=state,
            status=status,
            final=True,
            message=message,
            icon=icon,
            cli_color=cli_color,
            warnings=warnings,
            errors=errors,
        )
        yield finished

    def wait(self):
        state = None
        for s in self.watch():
            state = s
        return state

    def _get_field(self, field):
        """Retrieve an individual field from status.json"""
        self.get_status()
        return self.status.get(field, None)

    @property
    def warnings_count(self):
        """Get the warnings_count for the build"""
        return int(self._get_field("warnings_count"))

    @property
    def errors_count(self):
        """Get the errors_count for the build"""
        return int(self._get_field("errors_count"))

    @property
    def tuxbuild_status(self):
        """Get the tuxbuild_status for the build"""
        return self._get_field("state")

    @property
    def build_status(self):
        """Get the build_status for the build"""
        return self._get_field("result")

    @property
    def status_message(self):
        """Get the build_status for the build"""
        return self._get_field("status_message")


class BuildSet(HeadersMixin):
    def __init__(self, builds, **kwargs):
        self.builds = []
        for item in builds:
            data = kwargs.copy()
            data.update(item)
            self.builds.append(Build(**data))
        self.token = kwargs["token"]
        self.tuxapi_url = kwargs["tuxapi_url"]
        self.project = kwargs["project"]
        self.group = kwargs["group"]

    def build(self):
        """Submit the build request"""

        data = []
        url = self.tuxapi_url + "/v1/groups/{}/projects/{}/builds".format(
            self.group, self.project
        )
        for build_object in self.builds:
            data.append(build_object.generate_build_request())
        json_data = post_request(url, self.headers, data)

        builds_by_client_token = {b.client_token: b for b in self.builds}
        for build_config in json_data:
            build = builds_by_client_token[build_config["client_token"]]
            build.status = build_config
            build.build_data = build_config["download_url"]
            build.uid = build_config["uid"]

    def __watch_build__(self, build, results):
        for state in build.watch():
            results.put(state)

    def watch(self):
        results = queue.Queue()
        n = len(self.builds)

        with concurrent.futures.ThreadPoolExecutor(max_workers=n) as executor:
            futures = {}
            for build in self.builds:
                f = executor.submit(self.__watch_build__, build, results)
                futures[build.uid] = f

            finished = 0
            while finished < n:
                try:
                    state = results.get(timeout=60)
                except queue.Empty:
                    if all([f.done() for f in futures.values()]):
                        # Something went wrong: all threads finished but we
                        # don't have enough final states yet.
                        # Report any exceptions and don't wait forever
                        for build, f in futures.items():
                            exc = f.exception()
                            if exc:
                                tb = "".join(traceback.format_tb(exc.__traceback__))
                                print(
                                    f"ERROR for build {build}: {exc}\n{tb}",
                                    file=sys.stderr,
                                )
                        break
                else:
                    yield state
                    if state.final:
                        finished += 1

    def wait(self):
        states = []
        for s in self.watch():
            if s.final:
                states.append(s)
        return states

    @property
    def status_list(self):
        """Return a list of build status dictionaries"""
        return [build.status for build in self.builds]


class Plan(HeadersMixin):
    def __init__(self, config, **kwargs):
        self.config = config
        self.token = kwargs["token"]
        self.tuxapi_url = kwargs["tuxapi_url"]
        self.project = kwargs["project"]
        self.group = kwargs["group"]
        self.args = kwargs
        self.plan = kwargs.get("plan", None)
        self.builds = []
        self.tests = []
        self.status = {}
        self.url = self.tuxapi_url + "/v1/groups/{}/projects/{}".format(
            self.group, self.project
        )
        self.results = {"builds": {}, "tests": {}}

    def submit(self):
        builds = []
        tests = []
        builds_status = {}
        tests_status = {}

        # Create the plan
        self.plan = post_request(
            f"{self.url}/plans",
            self.headers,
            {"name": self.config.name, "description": self.config.description},
        )["uid"]

        # Create the builds
        for cfg in self.config.plan:
            if cfg["build"] is not None:
                data = self.args.copy()
                data.update(cfg["build"])
                builds.append(Build(**data))
            else:
                builds.append(None)

        builds_to_submit = [b for b in builds if b]
        if builds_to_submit:
            ret = post_request(
                f"{self.url}/builds",
                self.headers,
                [b.generate_build_request(plan=self.plan) for b in builds_to_submit],
            )
            # Updating builds_to_submit will update values in builds
            for (build, data) in zip(builds_to_submit, ret):
                build.build_data = data["download_url"]
                build.uid = data["uid"]
                build.status = data

        # Create the tests
        for (build, cfg) in zip(builds, self.config.plan):
            if build:
                for test in cfg["tests"]:
                    tests.append(
                        Test(
                            token=self.token,
                            kbapi_url="",
                            tuxapi_url=self.tuxapi_url,
                            group=self.group,
                            project=self.project,
                            device=test["device"],
                            kernel="",
                            tests=test.get("tests", []),
                            boot_args=test.get("boot_args"),
                            wait_for=build.uid,
                        )
                    )
                self.builds.append(build)
            else:
                for test in cfg["tests"]:
                    tests.append(
                        Test(
                            token=self.token,
                            kbapi_url="",
                            tuxapi_url=self.tuxapi_url,
                            group=self.group,
                            project=self.project,
                            device=test["device"],
                            kernel=test["kernel"],
                            modules=test.get("modules"),
                            tests=test.get("tests", []),
                            boot_args=test.get("boot_args"),
                        )
                    )

        ret = post_request(
            f"{self.url}/tests",
            self.headers,
            [t.generate_test_request(plan=self.plan) for t in tests],
        )
        for (test, data) in zip(tests, ret):
            test.uid = data["uid"]
            test.status = data
            self.tests.append(test)

        # Populate the status with builds and tests after submission
        for build in self.builds:
            builds_status.update({build.uid: build.status})
        for test in self.tests:
            tests_status.update({test.uid: test.status})
        self.status = {"builds": builds_status, "tests": tests_status}

    def get_plan(self):
        builds = {}
        tests = {}
        first = True
        start_builds = start_tests = None
        while first or start_builds is not None or start_tests is not None:
            ret = get_request(
                f"{self.url}/plans/{self.plan}",
                headers=self.headers,
                params={"start_builds": start_builds, "start_tests": start_tests},
            )
            if first or start_builds is not None:
                builds.update({b["uid"]: b for b in ret["builds"]["results"]})
                start_builds = ret["builds"]["next"]
            if first or start_tests is not None:
                tests.update({t["uid"]: t for t in ret["tests"]["results"]})
                start_tests = ret["tests"]["next"]

            first = False

        self.status = {"builds": builds, "tests": tests}
        return self.status

    def watch(self):
        n_builds = len(self.builds)
        n_tests = len(self.tests)

        while True:
            plan = self.get_plan()
            for build in [b for b in self.builds if b.uid in plan["builds"]]:
                if build.uid not in self.results["builds"]:
                    build.status = plan["builds"][build.uid]
                    if not hasattr(build, "ite"):
                        build.get_status = lambda: None
                        build.ite = build.watch(iterator=True)
                    state = next(build.ite)
                    if state is not None:
                        yield state
                        if state.final:
                            self.results["builds"][build.uid] = state
            for test in [t for t in self.tests if t.uid in plan["tests"]]:
                if test.uid not in self.results["tests"]:
                    test.get = lambda: plan["tests"][test.uid]
                    if not hasattr(test, "ite"):
                        test.ite = test.watch(iterator=True)
                    state = next(test.ite)
                    if state is not None:
                        yield state
                        if state.final:
                            self.results["tests"][test.uid] = state

            nr_builds = len(self.results["builds"])
            nr_tests = len(self.results["tests"])
            if nr_builds == n_builds and nr_tests == n_tests:
                break
            time.sleep(5)

    def wait(self):
        states = []
        for s in self.watch():
            if s.final:
                states.append(s)
        return states


@attrs(kw_only=True)
class Test(HeadersMixin):
    token = attrib()
    kbapi_url = attrib()
    tuxapi_url = attrib()
    group = attrib()
    project = attrib()
    device = attrib()
    kernel = attrib()
    modules = attrib(default=None)
    tests = attrib(default=[])
    boot_args = attrib(default=None)
    wait_for = attrib(default=None)
    uid = attrib(default=None)
    status = attrib(default={})
    url = attrib(default=None)

    def __str__(self):
        tests = "[" + ", ".join(["boot"] + self.tests) + "]"
        self.url = self.tuxapi_url + "/v1/groups/{}/projects/{}/tests/{}".format(
            self.group, self.project, self.uid
        )
        return "{} {} @ {}".format(tests, self.device, self.url)

    def generate_test_request(self, plan=None):
        data = {"device": self.device, "kernel": self.kernel}
        if self.modules:
            data["modules"] = self.modules
        if self.tests:
            data["tests"] = self.tests
        if self.boot_args:
            data["boot_args"] = self.boot_args
        if self.wait_for:
            data["waiting_for"] = self.wait_for
        if plan is not None:
            data["plan"] = plan

        return data

    def test(self, plan=None):
        url = self.tuxapi_url + "/v1/groups/{}/projects/{}/tests".format(
            self.group, self.project
        )
        data = self.generate_test_request(plan=plan)
        self.status = post_request(url, self.headers, data)
        self.uid = self.status["uid"]
        self.url = url + "/{}".format(self.uid)

    def get(self):
        self.status = get_request(self.url, self.headers)
        return self.status

    def watch(self, iterator=False):
        waiting_states = {
            "waiting": BuildState(
                build=self,
                state="waiting",
                message="Waiting",
                icon="⏳",
                cli_color="white",
            ),
            "provisioning": BuildState(
                build=self,
                state="provisioning",
                message="Provisioning",
                icon="⚙️ ",
                cli_color="white",
            ),
            "running": BuildState(
                build=self,
                state="running",
                message="Running",
                icon="🚀",
                cli_color="cyan",
            ),
        }

        previous_state = None
        while True:
            data = self.get()
            state = data["state"]
            if state != previous_state:
                if state in waiting_states:
                    yield waiting_states[state]
                elif state == "finished":
                    break
                else:
                    assert 0
            elif iterator:
                yield

            if not iterator:
                delay = random.randrange(delay_status_update)
                time.sleep(delay)
            previous_state = state

        errors = 0
        result = data["result"]
        if result == "pass":
            icon = "🎉"
            message = "Pass"
            cli_color = "green"
        elif result == "fail":
            icon = "👹"
            cli_color = "bright_red"
            errors = [
                name for name in data["results"] if data["results"][name] == "fail"
            ]
            message = "Fail ({})".format(", ".join(errors))
            errors = len(errors)
        else:
            icon = "🔧"
            cli_color = "bright_red"
            message = "TuxTest error"

        finished = BuildState(
            build=self,
            state=state,
            status=result,
            final=True,
            message=message,
            icon=icon,
            cli_color=cli_color,
            errors=errors,
        )
        yield finished

    def wait(self):
        state = None
        for s in self.watch():
            state = s
        return state


@attrs(kw_only=True)
class Results(HeadersMixin):
    token = attrib()
    kbapi_url = attrib()
    tuxapi_url = attrib()
    group = attrib()
    project = attrib()
    uid = attrib(default=None)

    def get_build(self):
        url = self.tuxapi_url + "/v1/groups/{}/projects/{}/builds/{}".format(
            self.group, self.project, self.uid
        )
        return get_request(url, self.headers)

    def get_test(self):
        url = self.tuxapi_url + "/v1/groups/{}/projects/{}/tests/{}".format(
            self.group, self.project, self.uid
        )
        result = get_request(url, self.headers)
        return (result, url)

    def get_plan(self):
        plan = Plan(
            None,
            token=self.token,
            tuxapi_url=self.tuxapi_url,
            project=self.project,
            group=self.group,
            plan=self.uid,
        )
        # TODO: Return the test url as part of the test result in tuxapi
        tuxapi_tests_url = plan.url + "/tests"
        return (plan.get_plan(), tuxapi_tests_url)

    def get_all(self):
        all_results = {}
        url = self.tuxapi_url + "/v1/groups/{}/projects/{}".format(
            self.group, self.project
        )
        build_url = url + "/builds"
        test_url = url + "/tests"
        plan_url = url + "/plans"
        all_results["builds"] = get_request(build_url, self.headers)
        all_results["tests"] = get_request(test_url, self.headers)
        all_results["plans"] = get_request(plan_url, self.headers)
        return (all_results, test_url)
